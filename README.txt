Clear End of Line
=================

Clear End of Line provides an input format filter that removes trailing white
spaces at end of lines.

Author:
  Thomas Barregren <http://drupal.org/user/16678>.

Sponsors:
  LRF Media <http://www.lrfmedia.se>.
  NodeOne <http://nodeone.se>.


BACKGROUND
----------

This input format filter can be used whenever you wish to remove spaces, tabs
and other white spaces that ends lines in a text area. This is of particular
interest if you use the Markdown filter <http://drupal.org/project/markdown>
and want to avoid that spaces mistakenly left at the end of lines become
line breaks, i.e. <br />, in the HTML output.


INSTALL
-------

See http://drupal.org/node/70151.


CONFIGURATION
-------------

To enable the filters provided by the module for a particular input format,
do as follows:

1. Go to admin/settings/filters.

2. Click on the configure link of the input format.

3. To enable the Clear End of Line filter, check "Clean end of line".

4. Click on the "Save configuration" button.

5. Click on the "Rearrange" tab.

6. Rearrange the filters such that "Clean end of line" is executed before
   the Markdown filter.



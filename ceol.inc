<?php



/**
 * Author:
 *   Thomas Barregren <http://drupal.org/user/16678>.
 */


/**
 * Returns a description of the Clean End of Line filter
 */
function _ceol_filter_description() {
  return t('Removes trailing white spaces at end of lines.');
}


/**
 * Returns the short filter tips.
 */
function _ceol_filter_short_tips() {
  return t('White spaces at the end of lines will be removed.');
}


/**
 * Returns the long filter tips.
 */
function _ceol_filter_long_tips() {
  return t('Spaces, tabs and other white spaces at the end of lines will be removed.');
}


/**
 * Removes white spaces at the end of lines.
 */
function _ceol_filter_process($text) {
  return preg_replace('/(.*?)\s*?(?:\n|\r)?$/m', '$1', $text);
}

